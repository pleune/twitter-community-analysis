#!/bin/bash
set -e

[ ! "$1" -ge "1" ] && echo "num parallel argument" && exit 1

PARALLEL=$1
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DDIR="${DIR}/data"
DDIR="${DIR}/results"
SEEDFILE="${DIR}/seed.txt"
FOLLOWERS_DIR="${DDIR}/user_followers/"
FOLLOWING_DIR="${DDIR}/user_following/"
FOS="${DDIR}/followers_of_seed.txt"
PDATA="${DDIR}/profile_data.csv"
GET_USER_LIST="${DIR}/build_user_list.sh"
FIND_RELEVANTE_ACCOUTNS="${DIR}/find_relevant_accounts"
DATAPACK="${DIR}/Data.tar.gz"
DATAPACK="${DIR}/Results.tar.gz"

mkdir -p "${DDIR}"
cp "${SEEDFILE}" "${DDIR}"

number_of_seeds=$(cat "${SEEDFILE}" | wc -l)
echo "Building data from ${number_of_seeds} seed accounts..."

[ -e "${FOLLOWERS_DIR}" ] && echo "${FOLLOWERS_DIR} Already exists!" && exit 1
[ -e "${FOS}" ] && echo "${FOS} Already exists!" && exit 1
[ -e "${FOSU}" ] && echo "${FOSU} Already exists!" && exit 1

"${GET_USER_LIST}" "${SEEDFILE}" $2 followers
"${GET_USER_LIST}" "${SEEDFILE}" $2 following
cat "$SEEDFILE" "${FOLLOWERS_DIR}"/* > "${FOS}"
cat "${FOS}" | sort | uniq > "${FOS}.uniq"
mv -f "${FOS}.uniq" "${FOS}"

"${GET_USER_LIST}" "${FOS}" $2 followers
"${GET_USER_LIST}" "${FOS}" $2 following

twint --userlist "${FOS}" --user-full --csv -o "${PDATA}"

echo $(cat "${FOS}" | wc -l) "Direct followers of seed found."
echo $(cat "${FOLLOWERS_DIR}"/* | wc -l) "Total followers found"
echo $(cat "${FOLLOWERS_DIR}"/* | sort | uniq | wc -l) "Total unique followers found"
echo $(cat "${FOLLOWING_DIR}"/* | wc -l) "Total following found"
echo $(cat "${FOLLOWING_DIR}"/* | sort | uniq | wc -l) "Total unique following found"

"${FIND_RELEVANT_ACCOUTNS}"

tar czf "${DATAPACK}" -C "${DIR}" "${DDIR}"
tar czf "${RESULTSPACK}" -C "${DIR}" "${RDIR}"
