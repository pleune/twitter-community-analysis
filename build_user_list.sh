#!/bin/bash

[[ ! -f "$1" ]] && echo "file of accounts as argument" && exit 1
[[ ! "$2" -ge 1 ]] && echo "number of parallel queries as second argument" && exit 1
[[ ! "$3" -eq "followers" ]] && [[ ! "$3" -eq "following" ]] && echo "followers/following as third argument" && exit 1

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
FDIR="$DIR/user_$3/"
mkdir -p "$FDIR"

# Try really hard not to leave processing running
trap 'sleep 1; kill $(jobs -p) 2>/dev/null' EXIT

max_children=$2
job_number=0
function parallel {
  let "job_number++"
  echo "starting ${job_number} ACC:$2..."
  sh -c "$@" || echo "finishing ${job_number}..." &

  local children=$(jobs -p | wc -l)
  children=$((children-1))
  while [[ ${children} -ge ${max_children} ]]; do
    # I COULD NOT get wait to actually wait. Idk if it's something
    # strange with having this many background jobs or what...
    #wait -n
    sleep 1
    children=$(jobs -p | wc -l)
    children=$((children-1))
  done
}


start_time="$(date +%s)"
for account in $(cat $1); do
    parallel \
      "if [ ! -f \"${FDIR}/${account}.txt\" ]; then                       \
        twint -u ${account} "--$3" > \"${FDIR}/${account}.txt\"; fi" "${account}"
done
wait
end_time="$(date +%s)"
elapsed=$((end_time-start_time))
echo "Finished ($elapsed seconds)."
