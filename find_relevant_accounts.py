#!/usr/bin/env python3

import networkx as nx
import os
import re
import csv
from io import open
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import ndimage

script_dir = os.path.dirname(os.path.realpath(__file__))

# req. files
user_followers_path = f'{script_dir}/data/user_followers'
user_following_path = f'{script_dir}/data/user_following'
followers_of_seed_path = f'{script_dir}/data/followers_of_seed.txt'
profile_data_path = f'{script_dir}/data/profile_data.csv'

# generated files
edges_path = f'{script_dir}/results/edges.txt'
enu_path = f'{script_dir}/results/enu.txt'
degrees_path = f'{script_dir}/results/degrees.txt'
results_path = f'{script_dir}/results/results.csv'
plot_distribution_path = f'{script_dir}/results/distribution.png'

if not os.path.exists(f'{script_dir}/results'):
    os.makedirs(f'{script_dir}/results')

# Load profile data
profile_data = {}
with open(profile_data_path, 'r', encoding='utf-8') as prof_data:
    csv_reader = csv.DictReader(prof_data, delimiter=',')
    line_count = 0
    for row in csv_reader:
        profile_data[row['username']] = row

# Load followers of seed
user_count = 1
user_to_enu = {}
enu_to_user = []  # must sub index by user_count inital index (e.g. 1)
with open(followers_of_seed_path, 'r') as fos:
    cleaning_filter = re.compile(r'\W+')  # a-z, A-Z, 0-9, and _ are allowed
    for line in fos:
        user = cleaning_filter.sub('', line)
        if user in profile_data and int(profile_data[user]['private']) == 0:
            user_to_enu[user] = user_count
            user_count += 1
            enu_to_user.append(user)


# Generate Edge List File
#
# Get a list of all the files in `data/user_followers` into `user_files`.
# Read through all of those files building onto `twit.edges.txt`
with open(edges_path, 'w') as file_edges:
    cleaning_filter = re.compile(r'\W+')  # a-z, A-Z, 0-9, and _ are allowed
    user_followers = [os.path.join(user_followers_path, f) for f in os.listdir(
        user_followers_path) if os.path.isfile(os.path.join(user_followers_path, f))]
    user_following = [os.path.join(user_following_path, f) for f in os.listdir(
        user_following_path) if os.path.isfile(os.path.join(user_following_path, f))]

    for file in user_followers:
        user, _ = os.path.splitext(os.path.basename(file))
        if not user in user_to_enu:
            continue
        user_enu = user_to_enu[user]
        with open(file, 'r') as f:
            followers = f.readlines()
        for line in followers:
            follower = cleaning_filter.sub('', line)
            if not follower in user_to_enu:
                continue
            follower_enu = user_to_enu[follower]
            file_edges.write('{}\t{}\n'.format(follower_enu, user_enu))

    for file in user_following:
        user, _ = os.path.splitext(os.path.basename(file))
        if not user in user_to_enu:
            continue
        user_enu = user_to_enu[user]
        with open(file, 'r') as f:
            followings = f.readlines()
        for line in followings:
            following = cleaning_filter.sub('', line)
            if not following in user_to_enu:
                continue
            following_enu = user_to_enu[following]
            file_edges.write('{}\t{}\n'.format(user_enu, following_enu))


# Generate a file that connects the enumerated values in the above file with actual usernames
with open(enu_path, 'w') as enu:
    for i in range(len(enu_to_user)):
        u = enu_to_user[i]
        enu.write(f'{i+1}\t{u}\n')

# Calculate the in and out degree for each node, and save it to disk
G = nx.read_edgelist(edges_path, create_using=nx.DiGraph())
with open(degrees_path, 'w') as f:
    for node in G.nodes(data=False):
        f.write('{} {} {}\n'.format(node, G.in_degree(node), G.out_degree(node)))

# Score each account
results = []
for user in enu_to_user:
    name = profile_data[user]['name']
    enu = user_to_enu[user]
    enu = str(enu)

    out_degree = int(G.out_degree(enu))
    in_degree = int(G.in_degree(enu))
    followers = int(profile_data[user]['followers'])
    following = int(profile_data[user]['following'])

    if following < 1 or followers < 1:
        continue

    # magic score
    # Important accounts are a combination of having a high percentage of followers
    # and following in the network, and a large number of total connections in the
    # network. Without any of these three things, the node is significantly less important.
    # For example, a node with zero followers but many many following should be extremly low
    # score, since it is most likely a spam account. For this reason, the three factors are multiplied.
    # They are then cube-rooted to keep the score linear.
    score = ((out_degree / followers) * (in_degree / following)
             * (out_degree + in_degree)) ** (1/3)
    results.append((user, score, name))

results = sorted(results, key=lambda x: x[1], reverse=True)

with open(results_path, 'w', encoding='utf-8') as resultsfile:
    resultsfile.write('Score,Username,Name\n')
    for r in results:
        resultsfile.write(f'{r[0]},{r[1]},{r[2]}\n')

# Graph scores
_, scores, _ = zip(*results)
plt.title('Scores')
plt.yscale('log')
plt.plot(range(len(scores)), scores)
plt.savefig(plot_distribution_path)

# Find where the scores start to plateau
# gaussian filter to prevent noise from triggering an early limit
limit = np.argmax(np.diff(ndimage.filters.gaussian_filter(scores, 1)))
print(f'The first {limit} results are most relevant')
